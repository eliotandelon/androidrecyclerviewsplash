package com.example.myapplication;

public class Programs {

    public Programs() {
    }

    public Programs(Integer image, String name, String description) {
        this.image = image;
        this.name = name;
        this.description = description;
    }

    private Integer image;
    private String name;
    private String description;

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
