package com.example.myapplication;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;
import java.util.List;

public class CustomAdapter extends  RecyclerView.Adapter<CustomAdapter.ViewHolder> {

    private final List<Programs> programs;

    public CustomAdapter() {
        programs = Arrays.asList(
                new Programs(R.drawable.admin_enter,
                        "Administración de empresas",
                        "Calle 30 No. 48 152 Avenida del Consulado, Piedra de Bolívar, Cartagena 130001, Colombia, Bolívar, Colombia\n Facultad de Ciencias Económicas "),
                new Programs(R.drawable.admin_indu,
                        "Administración industrial",
                        "Calle 30 Avenida del Consulado #No. 48 152, Piedra de Bolívar, Cartagena 130001, Colombia, Bolívar, Colombia\n Facultad de Ciencias Económicas "),
                new Programs(R.drawable.biologia,
                        "Biologia",
                        "Cra. 50 #24120, Campus San Pablo, Cartagena 130001, Colombia, Bolívar, Colombia\n Facultad de Ciencias Exactas y Naturales "),
                new Programs(R.drawable.com_social,
                        "Comunicaciòn Social",
                        "Cra. 6 #36-100, Claustro San Agustín, Cartagena 130001, Colombia, Bolívar, Colombia\n Facultad de Ciencias Sociales y Educación"),
                new Programs(R.drawable.cont_pub,
                        "Contaduria publica",
                        "Calle 30 Avenida del Consulado, No. 48 152, Piedra de Bolívar, Cartagena 130001, Colombia, Bolívar, Colombia\n Facultad de Ciencias Económicas"),
                new Programs(R.drawable.dere,
                        "Derecho",
                        "Cra. 6 #36-100, Claustro San Agustín, Cartagena 130001, Colombia, Bolívar, Colombia\n Facultad de Derecho y Ciencias Políticas"),
                new Programs(R.drawable.econ,
                        "Economia",
                        "Calle 30 No. 48 152 Avenida del Consulado, Piedra de Bolívar, Cartagena 130001, Colombia, Bolívar, Colombia\n Facultad de Ciencias Económicas "),
                new Programs(R.drawable.enfermeria,
                        "Enfermeria",
                        "Cra. 50 #24-120, Campus Zaragocilla, Cartagena 130001, Colombia, Bolívar, Colombia\n Facultad de Enfermería"),
                new Programs(R.drawable.filosofia,
                        "Filosofia",
                        "Cra. 6 #36-100, Claustro San Agustín, Cartagena 130001, Bolívar, Bolívar, Colombia\n Facultad de Ciencias Humanas"),
                new Programs(R.drawable.history,
                        "Historia",
                        "Cra. 6 #36-100, Claustro San Agustín, Cartagena 130001, Bolívar, Bolívar, Colombia\n Facultad de Ciencias Humanas"),
                new Programs(R.drawable.ing_civil,
                        "Ingenieria Civil",
                        "Ave. del Consulado No. 48 152, Piedra de Bolívar, Cartagena 130001, Colombia, Bolívar, Colombia\n Facultad de Ingeniería"),
                new Programs(R.drawable.ing_alime,
                        "Ingenieria de Alimentos",
                        "Ave. del Consulado No. 48 152, Piedra de Bolívar, Cartagena 130001, Colombia, Bolívar, Colombia\n Facultad de Ingeniería"),
                new Programs(R.drawable.ing_sist,
                        "Ingenieria De Sistemas",
                        "Ave. del Consulado No. 48 152, Piedra de Bolívar, Cartagena 130001, Colombia, Bolívar, Colombia\n Facultad de Ingeniería"),
                new Programs(R.drawable.ing_quimica,
                        "Ingenieria Quimica",
                        "Ave. del Consulado No. 48 152, Piedra de Bolívar, Cartagena 130001, Colombia, Bolívar, Colombia\n Facultad de Ingeniería"),
                new Programs(R.drawable.lic_educacion,
                        "Licenciatura en Educaciòn",
                        "Cra. 6 #36-100, Claustro San Agustín, Cartagena 130001, Colombia, Bolívar, Colombia\n Facultad de Ciencias Sociales y Educación"),
                new Programs(R.drawable.lin_lit,
                        "Lingüistica y Literatura",
                        "Cra. 6 #36-100, Claustro San Agustín, Cartagena 130001, Bolívar, Bolívar, Colombia\n Facultad de Ciencias Humanas"),
                new Programs(R.drawable.matematicas,
                        "Matematicas",
                        "Cra. 50 #24120, Campus San Pablo, Cartagena 130001, Colombia, Bolívar, Colombia\n Facultad de Ciencias Exactas y Naturales"),
                new Programs(R.drawable.medicina,
                        "Medicina",
                        "Carrera 50a #24 63, Campus Zaragocilla, Cartagena 130001, Colombia, Bolívar, Colombia\n Facultad de Medicina"),
                new Programs(R.drawable.odonto,
                        "Odontologia",
                        "Calle 6 #572, Campus Zaragocilla, Cartagena 130001, Colombia, Bolívar, Colombia\n Facultad de Odontología"),
                new Programs(R.drawable.lenguas_ex,
                        "Profesional en Lenguas Extranjeras",
                        "Cra. 50 #24120, Campus San Pablo, Cartagena 130001, Bolívar, Bolívar, Colombia\n Facultad de Ciencias Humanas"),
                new Programs(R.drawable.quimica,
                        "Quimica",
                        "Cra. 50 #24120, Campus San Pablo, Cartagena 130001, Colombia, Bolívar, Colombia\n Facultad de Ciencias Exactas y Naturales"),
                new Programs(R.drawable.quimi_farma,
                        "Quimica Farmaceutica",
                        "Cra. 50 #24-120, Campus Zaragocilla, Cartagena 130001, Colombia, Bolívar, Colombia\n Facultad de Ciencias Farmacéuticas"),
                new Programs(R.drawable.trabajo_social,
                        "Trabajo Social",
                        "Cra. 6 #36-100, Claustro San Agustín, Cartagena 130001, Colombia, Bolívar, Colombia\n Facultad de Ciencias Sociales y Educación"));
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.program_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Programs item = programs.get(position);
        holder.imgItem.setImageResource(item.getImage());
        holder.tvName.setText(item.getName());
        holder.tvDescription.setText(item.getDescription());
    }

    @Override
    public int getItemCount() {
        return programs.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView imgItem;
        private final TextView tvName;
        private final TextView tvDescription;

        public ViewHolder(View v) {
            super(v);
            imgItem = itemView.findViewById(R.id.imgItem);
            tvName = itemView.findViewById(R.id.tvTitulo);
            tvDescription = itemView.findViewById(R.id.tvDescripcion);
        }

        public ImageView getImgItem() {
            return imgItem;
        }

        public TextView getName() {
            return tvName;
        }

        public TextView getDescription() {
            return tvDescription;
        }
    }
}
